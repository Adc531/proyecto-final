# Proyecto Final

Coche radio-control con arduino

# Descripcion del proyecto

Coche con motor controlado desde un móvil android a través de arduino a través de una aplicación móvil con una tracción simple para poder girar el coche. 
Mediante arduino se puede programar para que vaya controlado tanto por ordenador como móvil, como mas le guste al usuario.

# Timeline

17/01/2022 - 21/02/2022
        Recopilación de páginas web de compra para los modulos de arduino.
        Información de los modulos.

24/01/2022 -  28/01/2022
        Recopilacion de toda la informacion de los modulos.
        Prueba de modulos con ejemplos de Adafruit.
        Empieza las pruebas en Android Studio tanto con kotlin como con Java.
        Empieza el diseño de la aplicacion movil
    
03/02/2022 - 04/02/2022
        Finalización del diseño de la aplicación V0.0.1
        Busqueda de Joystick para aplicacion movil
        Intento de implementación del joystick (Fallido // Kotlin)

07/02/2022 - 11/02/2022
        Intento de implementación del Joystick (Fallido // Java).
        Busqueda de Joystick.
        Busqueda de modulos de Arduino.
        Creacion del repositorio de GitLab y del Trello con el diagrama de Gantt.

14/02/2022 - 18/02/2022
        Documentacion de Arduino.
        Llegada de los primeros modulos
        Prueba de modulos con Arduino

21/02/2022 - 25/02/2022
        Llegada del motor y los servos.
        Prueba de servos con ejemplos de Arduino.
        Problema con el motor(Arreglado)
        Pre-Alpha 0.1 de la aplicacion movil

28/02/2022 - 06/03/2022
        Codificacion del sensor de infrarrojos
        Prueba de sensor infrarrojos con el motor y servo
        subida de programas a git
        Actualizacion de Trello 
        
07/03/2022 - 20-03-2022
        Pruebas exitosas de los sensores
        Codigo Arduino y Android Studio sobre la conexion

#  Informacion de modulos 

   Altavoz: https://www.amazon.es/AZDelivery-altavoz-piezoel%C3%A9ctrico-circuito-incluido/dp/B089QJKJXW/ref=sr_1_1_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&               crid=A4W0BA6ZQSWI&keywords=altavoz+arduino&qid=1643359088&sprefix=altavoz+arduino%2Caps%2C80&sr=8-1-spons&psc=1&smid=A1X7QLRQH87QA3&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUExN0dCUVo3MllINkVLJmVuY3J5cHRlZElkPUEwOTA4NzM1MldLRjg4MFJON1ZUSyZlbmNyeXB0ZWRBZElkPUEwNjkyMzUzMUgySDBSMzgwWjlDUCZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=
   Motor: https://www.luisllamas.es/motor-paso-paso-28byj-48-arduino-driver-uln2003/

   Luces LED: https://www.luisllamas.es/encender-un-led-con-arduino/

   Detector de distancia: https://www.luisllamas.es/medir-distancia-con-arduino-y-sensor-de-ultrasonidos-hc-sr04/

   Ruedas de coche de juguete: https://es.aliexpress.com/item/4000017467892.html?gatewayAdapt=glo2esp&_randl_currency=EUR&_randl_shipto=ES&src=google%2Chttps%3A%2F%2Fwww.aliexpress.com%2Fitem%2F4000017467892.html%3F_randl_currency%3DEUR&_randl_shipto=ES&src=google&src=google&albch=shopping&acnt=439-079-4345&slnk=&plac=&mtctp=&albbt=Google_7_shopping&gclsrc=aw.ds&albagn=888888&ds_e_adid=483620149093&ds_e_matchtype=&ds_e_device=c&ds_e_network=u&ds_e_product_group_id=539263010115&ds_e_product_id=es4000017467892&ds_e_product_merchant_id=107719616&ds_e_product_country=ES&ds_e_product_language=es&ds_e_product_channel=online&ds_e_product_store_id=&ds_url_v=2&ds_dest_url=https%3A%2F%2Fs.click.aliexpress.com%2Fdeep_link.htm%3Faff_short_key%3DUneMJZVf&albcp=11758679187&albag=116841987329&isSmbAutoCall=false&needSmbHouyi=false&gclid=Cj0KCQiAxc6PBhCEARIsAH8Hff0CLGLf0AuakxSRlozhRGEYtaAnmJ1z9B9rb7LbTfzioPl5zw24PIMaAp-rEALw_wcB&aff_fcid=2f29ca2aa41241fa9fe763c41437790d-1643360627348-06211-UneMJZVf&aff_fsk=UneMJZVf&aff_platform=aaf&sk=UneMJZVf&aff_trace_key=2f29ca2aa41241fa9fe763c41437790d-1643360627348-06211-UneMJZVf&terminal_id=3972ac8b741f4f428b3eb1392b36ba4c

   Controlador de motor: https://www.luisllamas.es/arduino-motor-corriente-continua-l298n/

   Banco de bateria:

   Interruptor: https://www.amazon.es/Interruptor-universal-off-Negro-Cablepelado%C2%AE/dp/B07CMG69BK/ref=sr_1_20?keywords=interruptor+arduino&qid=1643360807&sprefix=interruptor+ard%2Caps%2C135&sr=8-20

   Arduino Bluetooth: https://www.amazon.es/DSD-TECH-cl%C3%A1sico-Bluetooth-Bricolaje/dp/B09NKYV3D7/ref=sr_1_6?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=20COHMUNS6C08&keywords=arduino+bluetooth&qid=1643361103&sprefix=arduino+bluetooth%2Caps%2C106&sr=8-6

# TRELLO Y GANTT
https://trello.com/b/Tljv3nVW/proyecto-final
