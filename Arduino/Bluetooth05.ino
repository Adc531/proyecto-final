 ARDUINO   L293D(Puente H)        
 5          10
 6          15
 9          7
 10         2
 5V         1, 9, 16
 GND        4, 5, 12, 13
 
 //El motor 1 se conecta a los pines 3 y 6 del Puente H
 //El motor 2 se conecta a los pines 11 y 14 del Puente H
 
 Conexion del Modulo Bluetooth HC-06 y el Arduino
 ARDUINO    Bluetooth HC-06 
 0 (RX)       TX
 1 (TX)       RX
 5V           VCC
 GND          GND
// !!Cuidado!! Las conexiones de TX y RX al modulo Bluetooth deben estar desconectadas
// en el momento que se realiza la carga del codigo (Sketch) al Arduino.
 
 Conexion Sensor Ultrasonido HC-SR04
 ARDUINO    Ultrasonido HC-SR04 
 2            Echo
 3            Trig
 5V           VCC
 GND          Gnd

 Conexion Servo SG90
 ARDUINO    Ultrasonido HC-SR04 
 11           Signal
 5V           VCC
 GND          Gnd
 */

#include <Servo.h>                // Incluye la libreria Servo
Servo servo1;                    // Crea el objeto servo1 con las caracteristicas de Servo

int izqA = 5; 
int izqB = 6; 
int derA = 9; 
int derB = 10; 
int vel = 255;            // Velocidad de los motores (0-255)
int estado = 'c';         // inicia detenido
